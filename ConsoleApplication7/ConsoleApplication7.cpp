// ConsoleApplication7.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>

int main() {
    const int MAX_NOTES = 100; // Maximum number of notes
    float notes[MAX_NOTES]; // Array to store the notes
    int count = 0; // Number of notes entered
    float sum = 0; // Sum of all entered notes
    float note; // Temporary variable to store each note

    std::cout << "saisissez les notes (entre 0 et 20). Tapez -1 pour terminer." << std::endl;

    // Saisie des notes
    do {
        std::cout << "note #" << (count + 1) << ": ";
        std::cin >> note;

        if (note >= 0 && note <= 20) {
            if (note != -1) {
                notes[count] = note;
                sum += note;
                count++;
            }
        }
        else {
            std::cout << "la note doit être comprise entre 0 et 20. Réessayez." << std::endl;
        }
    } while (note != -1 && count < MAX_NOTES);

    // Calcul de la moyenne
    if (count > 0) {
        float average = sum / count;
        std::cout << "la moyenne des " << count << " notes saisies est : " << average << std::endl;
    }
    else {
        std::cout << "aucune note saisie." << std::endl;
    }
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
