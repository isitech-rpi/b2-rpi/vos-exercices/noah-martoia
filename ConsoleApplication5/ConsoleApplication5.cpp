// ConsoleApplication5.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
using namespace std;

int main() {
    int age;
    cout << "entrez l'âge de l'utilisateur : ";
    cin >> age;

    if (age >= 6 && age <= 9) {
        cout << "pré-poussin" << endl;
    }
    else if (age >= 10 && age <= 11) {
        cout << "poussin" << endl;
    }
    else if (age >= 12 && age <= 13) {
        cout << "benjamin" << endl;
    }
    else if (age >= 14 && age <= 15) {
        cout << "minime" << endl;
    }
    else if (age >= 16 && age <= 17) {
        cout << "cadet" << endl;
    }
    else if (age >= 18) {
        cout << "senior" << endl;
    }
    else {
        cout << "Âge invalide !" << endl;
    }
}


// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
