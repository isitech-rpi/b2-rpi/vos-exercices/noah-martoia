#include <iostream>
#include <string>

#include "consoleapplication15.A.cpp"

namespace Personnages {

    using namespace Armes;

    class Personnage {
    public:
        Personnage() : m_vie(100), m_nom("Jack") {}

        void recevoirDegats(int degats) {
            m_vie -= degats;
        }

        void coupDePoing(Personnage& cible) const {
            cible.recevoirDegats(10);
        }

        bool attaquer(Personnage& cible) {
            if (cible.estVivant()) {
                int degats = m_vie * m_niveau / 20;
                cible.recevoirDegats(degats);
                cible.defendre(*this);
                return true;
            }
            return false;
        }

        bool defendre(const Personnage& attaquant) {
            if (estVivant()) {
                int degats = attaquant.getVie() * attaquant.getNiveau() / 20;
                recevoirDegats(degats);
                return true;
            }
            return false;
        }

        bool estVivant() const {
            return m_vie > 0;
        }

        int getVie() const { return m_vie; }
        std::string getNom() const { return m_nom; }
        int getNiveau() const { return m_niveau; }

        void CollecterArme(const Arme& arme) {
            m_armeCourante = &arme;
        }

        bool Attaquer(Personnage& cible) {
                int degats = 50;
                cible.recevoirDegats(degats);
                cible.Defendre(*this);
                return true;
        }

        bool Defendre(const Personnage& attaquant) {
            if (m_armeCourante != nullptr) {
                int degats = attaquant.getVie() * attaquant.getNiveau() / 20;
                recevoirDegats(degats);
                return true;
            }
            return false;
        }

    private:
        int m_vie;
        std::string m_nom;
        int m_niveau = 1;
        const Arme* m_armeCourante = nullptr;
    };

    class Heros : public Personnage {
    public:
        Heros(const std::string& nom, int niveau)
            : Personnage(), m_niveau(niveau) {

        }
        int getNiveau() const { return m_niveau; }

        void EquiperPouvoir(const std::string& nomPouvoir, const Armes::Arme& arme) {}

    private:
        int m_niveau;
    };

}