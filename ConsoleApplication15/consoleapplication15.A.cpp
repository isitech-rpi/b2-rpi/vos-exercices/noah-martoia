#include <iostream>
#include <string>

namespace Armes {

    class Arme {
    public:
        Arme() = delete;
        virtual ~Arme() = default;

        virtual void Utilise() const = 0;
    protected:
        Arme(const std::string& nom, int attaque, int defense, int portee)
            : m_nom(nom), m_attaque(attaque), m_defense(defense), m_portee(portee) {}

        void Utilse(bool enAttaque) const {
            std::cout << "Arme : " << m_nom;
            if (enAttaque) {
                std::cout << " (Puissance d'attaque : " << m_attaque << ")";
            }
            else {
                std::cout << " (Puissance de d�fense : " << m_defense << ")";
            }
            std::cout << std::endl;
        }

    private:
        std::string m_nom;
        int m_attaque;
        int m_defense;
        int m_portee;
    };

}

namespace Armes {

    class Epee : public Arme {
    public:
        Epee() : Arme("�p�e", 20, 0, 2) {}

        void Utilise() const override {
            std::cout << "L'�p�e tranche avec puissance !" << std::endl;
        }
    };

    class Fourche : public Arme {
    public:
        Fourche() : Arme("Fourche", 10, 5, 3) {}

        void Utilise() const override {
            std::cout << "La fourche pique et prot�ge !" << std::endl;
        }
    };

}