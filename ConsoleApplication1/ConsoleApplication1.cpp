// ConsoleApplication1.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <iomanip>

int main()
{
    int a = 1;
    int b = 5;
    float f = 3.14;
    char c = 'C';
    char d;
    std::cout << "a = " << std::hex << a << std::endl;
    std::cout << "b = " << std::hex << b << std::endl;
    std::cout << "f = " << std::hex << f << std::endl;
    std::cout << "c = " << std::hex << c << std::endl;
    
    d = 'd';
    std::cout << "d = " << d << ":" << std::hex << d << std::endl;

    int stock = a;
    a = b;
    b = stock;
    std::cout << "a = " << std::hex << a << std::endl;
    std::cout << "b = " << std::hex << b << std::endl;

    int stock2 = a;
    f = a;
    f = stock2;
    std::cout << "a = " << std::hex << a << std::endl;
    std::cout << "f = " << std::hex << f << std::endl;

    d = 90;
    std::cout << "d = " << static_cast<int>(d) << std::endl;

    d += 255;
    std::cout << "d = " << static_cast<int>(d) << std::endl;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
