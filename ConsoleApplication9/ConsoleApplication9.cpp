// ConsoleApplication9.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <vector>
#include <ctime>
#include <cstdlib>

std::string tireUneCarte(int nbCartes) {
    std::vector<std::string> cartes;

    if (nbCartes == 32) {
        cartes = { 
            "as_coeur","7_coeur", "8_coeur", "9_coeur", "10_coeur","valet_coeur","dame_coeur","roi_coeur", 
            "as_pique","7_pique", "8_pique", "9_pique", "10_pique","valet_pique","dame_pique","roi_pique", 
            "as_trefles","7_trefles", "8_trefles", "9_trefles", "10_trefles","valet_trefles","dame_trefles","roi_trefles", 
            "as_carreaux","7_carreaux", "8_carreaux", "9_carreaux", "10_carreaux","valet_carreaux","dame_carreaux","roi_carreaux", 
        };
    }
    else if (nbCartes == 54) {
        cartes = {
            "as_coeur","2_coeur", "3_coeur", "4_coeur", "5_coeur","6_coeur","7_coeur", "8_coeur", "9_coeur", "10_coeur","valet_coeur","dame_coeur","roi_coeur",
            "as_pique","2_pique", "3_pique", "4_pique", "5_pique","6_pique","7_pique", "8_pique", "9_pique", "10_pique","valet_pique","dame_pique","roi_pique",
            "as_trefles","2_trefles", "3_trefles", "4_trefles", "5_trefles","6_trefles","7_trefles", "8_trefles", "9_trefles", "10_trefles","valet_trefles","dame_trefles","roi_trefles",
            "as_carreaux","2_carreaux", "3_carreaux", "4_carreaux", "5_carreaux","6_carreaux","7_carreaux", "8_carreaux", "9_carreaux", "10_carreaux","valet_carreaux","dame_carreaux","roi_carreaux",
            "joker1", "joker2"
        };
    }
    else {
        return "changer la variable typejeu par 32 ou 54";
    }

    std::srand(std::time(0));
    int index = std::rand() % cartes.size();
    return cartes[index];
}

int main() {
    // changer 54 par 32 pour avoir le jeux de 32 cartes
    int typeJeu = 54;
    std::string carteTiree = tireUneCarte(typeJeu);
    std::cout << carteTiree << std::endl;

    return 0;
}

// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
