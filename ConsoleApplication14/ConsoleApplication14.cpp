// ConsoleApplication14.cpp : Ce fichier contient la fonction 'main'. L'exécution du programme commence et se termine à cet endroit.
//

#include <iostream>
#include <string>

class Etudiant {
private:
    std::string nom;
    double* notes;

public:
    Etudiant() : nom(""), notes(nullptr) {}

    Etudiant(const std::string& nom) : nom(nom), notes(nullptr) {}

    ~Etudiant() {
        delete[] notes;
    }

    std::string GetNom() const {
        return nom;
    }

    void SetNom(const std::string& nouveauNom) {
        nom = nouveauNom;
    }

    bool ajouteNote(const int indice, double val) {
        if (indice >= 0 && indice < 10 && val >= 0 && val <= 20) {
            if (!notes) {
                notes = new double[10];
                for (int i = 0; i < 10; ++i) {
                    notes[i] = -1;
                }
            }
            notes[indice] = val;
            return true;
        }
        return false;
    }

    void AfficheNotes() const {
        std::cout << "Notes valides pour " << nom << ":" << std::endl;
        for (int i = 0; i < 10; ++i) {
            if (notes[i] >= 0 && notes[i] <= 20) {
                std::cout << "Indice " << i << ": " << notes[i] << std::endl;
            }
        }
    }

    double Moyenne() const {
        double somme = 0;
        int count = 0;
        for (int i = 0; i < 10; ++i) {
            if (notes[i] >= 0 && notes[i] <= 20) {
                somme += notes[i];
                ++count;
            }
        }
        return (count > 0) ? (somme / count) : 0.0;
    }
};

class Cours {
private:
    std::string matiere;
    std::string promo;

public:
    Cours() {
        matiere = "";
        promo = "";
    }

    Cours(const std::string& matiere, const std::string& promo) {
        this->matiere = matiere;
        this->promo = promo;
    }

    ~Cours() {
        std::cout << "Objet Cours détruit." << std::endl;
    }

    std::string GetMatiere() const {
        return matiere;
    }

    std::string GetPromo() const {
        return promo;
    }

    void SetPromo(const std::string& nouvellePromo) {
        promo = nouvellePromo;
    }

    void Enseigner() const {
        std::cout << "Enseigne " << matiere << " à la promotion " << promo << std::endl;
    }
};

class Enseignant {
private:
    std::string nom;
    Cours* cours;

public:
    Enseignant() : nom(""), cours(nullptr) {}
    Enseignant(const std::string& nom) : nom(nom), cours(nullptr) {}
    ~Enseignant() {
        delete cours;
    }

    std::string GetNom() const {
        return nom;
    }

    void SetNom(const std::string& nouveauNom) {
        nom = nouveauNom;
    }

    void SetCours(Cours* nouveauCours) {
        cours = nouveauCours;
    }

    void FaireCours() const {
        if (cours) {
            std::cout << nom << " donne cours : ";
            cours->Enseigner();
        }
        else {
            std::cout << nom << " ne donne aucun cours." << std::endl;
        }
    }
};

int main() {
    Cours matiere_1("c++", "b2-RPI");

    Cours* matiere_2 = new Cours("PHP", "3olen");

    Cours lesCours_1[2] = { Cours("UWP", "B3-WTech"), Cours("UML", "b2-devWeb") };

    Cours* lesCours_2 = new Cours[2];
    lesCours_2[0] = Cours("PHP", "B2-WTech");
    lesCours_2[1] = Cours("PHP", "B2-WTech");
    for (const auto& cours : lesCours_1) {
        std::cout << "Matiere: " << cours.GetMatiere() << ", Promo: " << cours.GetPromo() << std::endl;
    }

    for (int i = 0; i < 2; ++i) {
        std::cout << "Matiere: " << lesCours_2[i].GetMatiere() << ", Promo: " << lesCours_2[i].GetPromo() << std::endl;
    }

    lesCours_1[0].SetPromo("B3-Info");
    lesCours_2[0].SetPromo("B2-DevOps");
    lesCours_2[1].SetPromo("B2-AI");

    delete matiere_2;
    delete[] lesCours_2;


    Etudiant etudiantStatique("Alice");
    etudiantStatique.ajouteNote(0, 15.5);
    etudiantStatique.ajouteNote(1, 18.0);
    etudiantStatique.AfficheNotes();
    std::cout << "Moyenne: " << etudiantStatique.Moyenne() << std::endl;

    Etudiant* etudiantDynamique = new Etudiant("Bob");
    etudiantDynamique->ajouteNote(0, 12.0);
    etudiantDynamique->ajouteNote(1, 14.5);
    etudiantDynamique->AfficheNotes();
    std::cout << "Moyenne: " << etudiantDynamique->Moyenne() << std::endl;

    delete etudiantDynamique;

    Cours cours1("C++", "B2-RPI");
    Enseignant enseignantStatique("Professeur A");
    enseignantStatique.SetCours(&cours1);
    enseignantStatique.FaireCours();

    Cours cours2("UML", "B3-Info");
    Enseignant* enseignantDynamique = new Enseignant("Professeur B");
    enseignantDynamique->SetCours(&cours2);
    enseignantDynamique->FaireCours();

    delete enseignantDynamique;

}



// Exécuter le programme : Ctrl+F5 ou menu Déboguer > Exécuter sans débogage
// Déboguer le programme : F5 ou menu Déboguer > Démarrer le débogage

// Astuces pour bien démarrer : 
//   1. Utilisez la fenêtre Explorateur de solutions pour ajouter des fichiers et les gérer.
//   2. Utilisez la fenêtre Team Explorer pour vous connecter au contrôle de code source.
//   3. Utilisez la fenêtre Sortie pour voir la sortie de la génération et d'autres messages.
//   4. Utilisez la fenêtre Liste d'erreurs pour voir les erreurs.
//   5. Accédez à Projet > Ajouter un nouvel élément pour créer des fichiers de code, ou à Projet > Ajouter un élément existant pour ajouter des fichiers de code existants au projet.
//   6. Pour rouvrir ce projet plus tard, accédez à Fichier > Ouvrir > Projet et sélectionnez le fichier .sln.
